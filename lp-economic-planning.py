# Code designed to simulate a simplified central economic planning regimen.
# This is in order to asses computational complexity limits.

#initial start from: https://hackernoon.com/linear-programming-in-python-a-straight-forward-tutorial-a0d152618121 


# currently using pulp: https://github.com/coin-or/pulp 
# alternatively could try scipy: https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linprog.html 

#first: pip install pulp

from pulp import *
import time


#to time program
start_time = time.time()




problem = LpProblem("problemName", LpMaximize)

# factory cost per day
cf0 = 450
cf1 = 420
cf2 = 400

# factory throughput per day
f0 = 2000
f1 = 1500
f2 = 1000

# production goal
goal = 80000

# time limit
max_num_days = 30

#factories
num_factories = 3

#decision variables
factory_days = LpVariable.dicts("factoryDays", list(range(num_factories)), 0, 30, cat="Continuous")

# goal constraint (article had error of f1-3 not zero indexed here)
c1 = factory_days[0]*f0 + factory_days[1]*f1 + factory_days[2] * f2 >= goal

#The constraints that we care about are that the number of units assembled should be above or equal to the goal amount and the production constraint that no factory should produce more than double as the other factory:

# production constraints
c2 = factory_days[0]*f0 <= 2*factory_days[1]*f1
c3 = factory_days[0]*f0 <= 2*factory_days[2]*f2
c4 = factory_days[1]*f1 <= 2*factory_days[2]*f2
c5 = factory_days[1]*f1 <= 2*factory_days[0]*f0
c6 = factory_days[2]*f2 <= 2*factory_days[1]*f1
c7 = factory_days[2]*f2 <= 2*factory_days[0]*f0

# production constraints
c2 = factory_days[0]*f0 <= 2*factory_days[1]*f1
c3 = factory_days[0]*f0 <= 2*factory_days[2]*f2
c4 = factory_days[1]*f1 <= 2*factory_days[2]*f2
c5 = factory_days[1]*f1 <= 2*factory_days[0]*f0
c6 = factory_days[2]*f2 <= 2*factory_days[1]*f1
c7 = factory_days[2]*f2 <= 2*factory_days[0]*f0


# adding the constraints to the problem
problem += c1
problem += c2
problem += c3
problem += c4
problem += c5
problem += c6
problem += c7


#The objective function for the computer assembly problem is basically minimizing the cost of assembling all of those computers. This can be written simply as maximizing the negative cost:

# objective function
problem += -factory_days[0]*cf0*f0 - factory_days[1]*cf1*f1 - factory_days[2]*cf2*f2

print("PROBLEM: ")
print(problem)

# solving
problem.solve()

print(" ")
print("SOLUTION: ")

for i in range(3):
 print(f"Factory {i}: {factory_days[i].varValue}")

print(" ")
print("Time to solve: ")
print("--- %s seconds ---" % (time.time() - start_time))

